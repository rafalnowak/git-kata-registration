require 'date'
require 'erb'
require 'json'
require 'net/https'
require 'uri'

github_uri = URI.parse 'https://api.github.com/'
github_api = Net::HTTP.new github_uri.host, github_uri.port
github_api.use_ssl = true
profile = JSON.parse github_api.get('/users/tomasz').body

ohai_uri = URI.parse 'https://gist.github.com/tomasz/0558ce648c7497380283/raw/53cbf5533a0f6309efa97ce015d4852226e6597d/ohai.txt.erb'
ohai = ERB.new(Net::HTTP.get ohai_uri).result

puts ohai
